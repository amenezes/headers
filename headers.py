#!/usr/bin/env python

from lib.core import headers

def run():
    app.main()

if __name__ == "__main__":
    app = headers.Headers()
    run()
